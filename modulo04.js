/*
var minhaPromise = function(){
  return new Promise(function(resolve,reject){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://gitlab.com/api/v4/users?username=hsikorski', true);
    xhr.send(null);

    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          resolve(JSON.parse(xhr.responseText));
        } else {
          reject('Deu erro em tudo');
        }        
      }
    }
  })
}

minhaPromise()
  .then(function(response){
    console.log(response);
  })
  .catch(function(error){
    console.warn(error);
  });
*/

var listElement = document.querySelector('#app ul');
var inputElement = document.querySelector('#app input');
var buttonElement = document.querySelector('#app button');
var imageElement = document.querySelector('#app img');
var msgError = document.getElementById('error');
var msgLoading = document.getElementById('loading');

msgError.style.display = 'none';
msgLoading.style.display = 'none';

function loadImg(src,name){
  imageElement.src = src;
  imageElement.alt = `Imagem de ${name}`;
  imageElement.style.width = '150px';
  imageElement.style.height = '150px';
  imageElement.style.display = 'block';
}
function searchUser(){
  msgLoading.style.display = 'block';
  msgError.style.display = 'none';
  imageElement.style.display = 'none';

  axios.get('https://api.github.com/users/'+inputElement.value)
  .then(function(response){
    msgLoading.style.display = 'none';
    loadImg(response.data.avatar_url, response.data.name);
  })
  .catch(function(error){
    msgLoading.style.display = 'none';
    msgError.style.display = 'block';
  });

}

buttonElement.onclick = searchUser;

